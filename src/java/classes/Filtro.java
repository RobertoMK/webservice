package classes;

public class Filtro {
    String dt_inicial;
    String dt_final;
    int colecao;
    int tp_situacao [];
    int marca;
    int cd_represent;

    public Filtro() {
    }

    public Filtro(String dt_inicial, String dt_final, int colecao, int[] tp_situacao, int marca, int cd_represent) {
        this.dt_inicial = dt_inicial;
        this.dt_final = dt_final;
        this.colecao = colecao;
        this.tp_situacao = tp_situacao;
        this.marca = marca;
        this.cd_represent = cd_represent;
    }

    public String getDt_inicial() {
        return dt_inicial;
    }

    public void setDt_inicial(String dt_inicial) {
        this.dt_inicial = dt_inicial;
    }

    public String getDt_final() {
        return dt_final;
    }

    public void setDt_final(String dt_final) {
        this.dt_final = dt_final;
    }

    public int getColecao() {
        return colecao;
    }

    public void setColecao(int colecao) {
        this.colecao = colecao;
    }

    public int[] getTp_situacao() {
        return tp_situacao;
    }

    public void setTp_situacao(int[] tp_situacao) {
        this.tp_situacao = tp_situacao;
    }

    public int getMarca() {
        return marca;
    }

    public void setMarca(int marca) {
        this.marca = marca;
    }

    public int getCd_represent() {
        return cd_represent;
    }

    public void setCd_represent(int cd_represent) {
        this.cd_represent = cd_represent;
    }

   
    
    
}
