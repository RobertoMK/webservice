package classes;

public class ProdutoHistorico {
    private String ds_grupo;
    private String qtd_solicitada;
    private String valor;

    public ProdutoHistorico() {
    }

    public ProdutoHistorico(String ds_grupo, String qtd_solicitada, String valor) {
        this.ds_grupo = ds_grupo;
        this.qtd_solicitada = qtd_solicitada;
        this.valor = valor;
    }

    public String getDs_grupo() {
        return ds_grupo;
    }

    public void setDs_grupo(String ds_grupo) {
        this.ds_grupo = ds_grupo;
    }

    public String getQtd_solicitada() {
        return qtd_solicitada;
    }

    public void setQtd_solicitada(String qtd_solicitada) {
        this.qtd_solicitada = qtd_solicitada;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
