package classes;

public class Composicao {
    private int pr_composicao;
    private String ds_fibra;
    
    public Composicao() {
    }

    public Composicao(int pr_composicao, String ds_fibra) {
        this.pr_composicao = pr_composicao;
        this.ds_fibra = ds_fibra;
    }

    public int getPr_composicao() {
        return pr_composicao;
    }

    public void setPr_composicao(int pr_composicao) {
        this.pr_composicao = pr_composicao;
    }

    public String getDs_fibra() {
        return ds_fibra;
    }

    public void setDs_fibra(String ds_fibra) {
        this.ds_fibra = ds_fibra;
    }
    
}
