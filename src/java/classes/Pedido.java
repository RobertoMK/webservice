package classes;

public class Pedido {
    private String cd_empresa;
    String nome_repre;
    String cd_pedido;
    String ped_cliente;
    String qt_solicitada;
    String qt_atendida;
    String qt_extra;
    String qt_cancelada;
    String qt_pedente;
    String qt_real;
    String vl_solicitado;
    String vl_solicitadoneg;
    String vl_atendido;
    String vl_atendidoneg;
    String vl_cancelado;
    String vl_canceladoneg;
    String vl_pedente;
    String vl_pedenteneg;
    String vl_extra;
    String vl_extraneg;
    String vl_real;
    String vl_realneg;
    String colecao;
    String cd_cliente;
    String nm_cliente;
    String cpnj_cliente;
    String dt_pedido;
    String dt_prev_fat;
    String tp_situacao;
    String nr_item;

    public Pedido() {
    }

    public Pedido(String cd_empresa, String nome_repre, String cd_pedido, String ped_cliente, String qt_solicitada, String qt_atendida, String qt_extra, String qt_cancelada, String qt_pedente, String qt_real, String vl_solicitado, String vl_solicitadoneg, String vl_atendido, String vl_atendidoneg, String vl_cancelado, String vl_canceladoneg, String vl_pedente, String vl_pedenteneg, String vl_extra, String vl_extraneg, String vl_real, String vl_realneg, String colecao, String cd_cliente, String nm_cliente, String cpnj_cliente, String dt_pedido, String dt_prev_fat, String tp_situacao, String nr_item) {
        this.cd_empresa = cd_empresa;
        this.nome_repre = nome_repre;
        this.cd_pedido = cd_pedido;
        this.ped_cliente = ped_cliente;
        this.qt_solicitada = qt_solicitada;
        this.qt_atendida = qt_atendida;
        this.qt_extra = qt_extra;
        this.qt_cancelada = qt_cancelada;
        this.qt_pedente = qt_pedente;
        this.qt_real = qt_real;
        this.vl_solicitado = vl_solicitado;
        this.vl_solicitadoneg = vl_solicitadoneg;
        this.vl_atendido = vl_atendido;
        this.vl_atendidoneg = vl_atendidoneg;
        this.vl_cancelado = vl_cancelado;
        this.vl_canceladoneg = vl_canceladoneg;
        this.vl_pedente = vl_pedente;
        this.vl_pedenteneg = vl_pedenteneg;
        this.vl_extra = vl_extra;
        this.vl_extraneg = vl_extraneg;
        this.vl_real = vl_real;
        this.vl_realneg = vl_realneg;
        this.colecao = colecao;
        this.cd_cliente = cd_cliente;
        this.nm_cliente = nm_cliente;
        this.cpnj_cliente = cpnj_cliente;
        this.dt_pedido = dt_pedido;
        this.dt_prev_fat = dt_prev_fat;
        this.tp_situacao = tp_situacao;
        this.nr_item = nr_item;
    }

    public String getCd_empresa() {
        return cd_empresa;
    }

    public void setCd_empresa(String cd_empresa) {
        this.cd_empresa = cd_empresa;
    }

    public String getNome_repre() {
        return nome_repre;
    }

    public void setNome_repre(String nome_repre) {
        this.nome_repre = nome_repre;
    }
    
    public String getCd_pedido() {
        return cd_pedido;
    }

    public void setCd_pedido(String cd_pedido) {
        this.cd_pedido = cd_pedido;
    }

    public String getPed_cliente() {
        return ped_cliente;
    }

    public void setPed_cliente(String ped_cliente) {
        this.ped_cliente = ped_cliente;
    }

    public String getQt_solicitada() {
        return qt_solicitada;
    }

    public void setQt_solicitada(String qt_solicitada) {
        this.qt_solicitada = qt_solicitada;
    }

    public String getQt_atendida() {
        return qt_atendida;
    }

    public void setQt_atendida(String qt_atendida) {
        this.qt_atendida = qt_atendida;
    }

    public String getQt_extra() {
        return qt_extra;
    }

    public void setQt_extra(String qt_extra) {
        this.qt_extra = qt_extra;
    }

    public String getQt_cancelada() {
        return qt_cancelada;
    }

    public void setQt_cancelada(String qt_cancelada) {
        this.qt_cancelada = qt_cancelada;
    }

    public String getQt_pedente() {
        return qt_pedente;
    }

    public void setQt_pedente(String qt_pedente) {
        this.qt_pedente = qt_pedente;
    }

    public String getQt_real() {
        return qt_real;
    }

    public void setQt_real(String qt_real) {
        this.qt_real = qt_real;
    }

    public String getVl_solicitado() {
        return vl_solicitado;
    }

    public void setVl_solicitado(String vl_solicitado) {
        this.vl_solicitado = vl_solicitado;
    }

    public String getVl_solicitadoneg() {
        return vl_solicitadoneg;
    }

    public void setVl_solicitadoneg(String vl_solicitadoneg) {
        this.vl_solicitadoneg = vl_solicitadoneg;
    }

    public String getNr_item() {
        return nr_item;
    }

    public void setNr_item(String nr_item) {
        this.nr_item = nr_item;
    }

    public String getVl_atendidoneg() {
        return vl_atendidoneg;
    }

    public void setVl_atendidoneg(String vl_atendidoneg) {
        this.vl_atendidoneg = vl_atendidoneg;
    }

    public String getVl_canceladoneg() {
        return vl_canceladoneg;
    }

    public void setVl_canceladoneg(String vl_canceladoneg) {
        this.vl_canceladoneg = vl_canceladoneg;
    }

    public String getVl_pedenteneg() {
        return vl_pedenteneg;
    }

    public void setVl_pedenteneg(String vl_pedenteneg) {
        this.vl_pedenteneg = vl_pedenteneg;
    }

    public String getVl_extraneg() {
        return vl_extraneg;
    }

    public void setVl_extraneg(String vl_extraneg) {
        this.vl_extraneg = vl_extraneg;
    }

    public String getVl_realneg() {
        return vl_realneg;
    }

    public void setVl_realneg(String vl_realneg) {
        this.vl_realneg = vl_realneg;
    }

    public String getColecao() {
        return colecao;
    }

    public void setColecao(String colecao) {
        this.colecao = colecao;
    }

    public String getCd_cliente() {
        return cd_cliente;
    }

    public void setCd_cliente(String cd_cliente) {
        this.cd_cliente = cd_cliente;
    }

    public String getNm_cliente() {
        return nm_cliente;
    }

    public void setNm_cliente(String nm_cliente) {
        this.nm_cliente = nm_cliente;
    }

    public String getDt_prev_fat() {
        return dt_prev_fat;
    }

    public void setDt_prev_fat(String dt_prev_fat) {
        this.dt_prev_fat = dt_prev_fat;
    }

    public String getCpnj_cliente() {
        return cpnj_cliente;
    }

    public void setCpnj_cliente(String cpnj_cliente) {
        this.cpnj_cliente = cpnj_cliente;
    }

    public String getDt_pedido() {
        return dt_pedido;
    }

    public void setDt_pedido(String dt_pedido) {
        this.dt_pedido = dt_pedido;
    }

    public String getTp_situacao() {
        return tp_situacao;
    }

    public void setTp_situacao(String tp_situacao) {
        this.tp_situacao = tp_situacao;
    }

    public String getVl_atendido() {
        return vl_atendido;
    }

    public void setVl_atendido(String vl_atendido) {
        this.vl_atendido = vl_atendido;
    }

    public String getVl_cancelado() {
        return vl_cancelado;
    }

    public void setVl_cancelado(String vl_cancelado) {
        this.vl_cancelado = vl_cancelado;
    }

    public String getVl_pedente() {
        return vl_pedente;
    }

    public void setVl_pedente(String vl_pedente) {
        this.vl_pedente = vl_pedente;
    }

    public String getVl_extra() {
        return vl_extra;
    }

    public void setVl_extra(String vl_extra) {
        this.vl_extra = vl_extra;
    }

    public String getVl_real() {
        return vl_real;
    }

    public void setVl_real(String vl_real) {
        this.vl_real = vl_real;
    }
    
    
}