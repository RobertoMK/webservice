package classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;

public class Token {

    public Token() {
    }

    public boolean create_token(String token) {
        String ver = check_token(token);
        int ff = Integer.valueOf(reader_check());
        String aute = "lojapwbrasilpwbrasilgrupopwbrasil" + ff;

        if (ver.equals(aute)) {
            writer_check(ff+1);
            return true;
        } else {
            return false;
        }
    }

    public String check_token(String token) {
        byte[] decodedValue = Base64.getDecoder().decode(token);
        return new String(decodedValue, StandardCharsets.UTF_8);
    }

    public String reader_check() {
        String linha = "";
        Scanner ler = new Scanner(System.in);
        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\alexandrefc\\Documents\\NetBeansProjects\\webservices\\src\\java\\classes\\check.txt"));

            while (br.ready()) {
                linha = br.readLine();
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return linha;
    }

    public void writer_check(int vv) {
        try {

            FileWriter arq = new FileWriter("C:\\Users\\alexandrefc\\Documents\\NetBeansProjects\\webservices\\src\\java\\classes\\check.txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            
            gravarArq.printf(vv+"");

            arq.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
