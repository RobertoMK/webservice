package classes;

public class ProdutoSobra {
    private String cd_produto;
    private String vl_formula_1;
    private String qt_movimentada;
    private String ds_cor;
    private String ds_tamanho;
    private String ds_sobra;
    
    public ProdutoSobra() {
    }

    public ProdutoSobra(String cd_produto, String vl_formula_1, String qt_movimentada, String ds_cor, String ds_tamanho, String ds_sobra) {
        this.cd_produto = cd_produto;
        this.vl_formula_1 = vl_formula_1;
        this.qt_movimentada = qt_movimentada;
        this.ds_cor = ds_cor;
        this.ds_tamanho = ds_tamanho;
        this.ds_sobra = ds_sobra;
    }

    public String getCd_produto() {
        return cd_produto;
    }

    public void setCd_produto(String cd_produto) {
        this.cd_produto = cd_produto;
    }

    public String getVl_formula_1() {
        return vl_formula_1;
    }

    public void setVl_formula_1(String vl_formula_1) {
        this.vl_formula_1 = vl_formula_1;
    }

    public String getQt_movimentada() {
        return qt_movimentada;
    }

    public void setQt_movimentada(String qt_movimentada) {
        this.qt_movimentada = qt_movimentada;
    }

    public String getDs_cor() {
        return ds_cor;
    }

    public void setDs_cor(String ds_cor) {
        this.ds_cor = ds_cor;
    }

    public String getDs_tamanho() {
        return ds_tamanho;
    }

    public void setDs_tamanho(String ds_tamanho) {
        this.ds_tamanho = ds_tamanho;
    }

    public String getDs_sobra() {
        return ds_sobra;
    }

    public void setDs_sobra(String ds_sobra) {
        this.ds_sobra = ds_sobra;
    }
    
    
}
