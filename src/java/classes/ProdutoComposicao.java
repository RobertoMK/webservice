package classes;

import java.util.ArrayList;

public class ProdutoComposicao {
    private String cd_produto;
    private ArrayList<Composicao> comp;

    public ProdutoComposicao() {
    }

    public ProdutoComposicao(String cd_produto, ArrayList<Composicao> comp) {
        this.cd_produto = cd_produto;
        this.comp = comp;
    }

    public String getCd_produto() {
        return cd_produto;
    }

    public void setCd_produto(String cd_produto) {
        this.cd_produto = cd_produto;
    }

    public ArrayList<Composicao> getComp() {
        return comp;
    }

    public void setComp(ArrayList<Composicao> comp) {
        this.comp = comp;
    }
}
