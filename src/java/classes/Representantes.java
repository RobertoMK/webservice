package classes;

public class Representantes {

    private String cd_representante;
    private String nome;

    public Representantes() {
    }

    public Representantes(String cd_representante, String nome) {
        this.cd_representante = cd_representante;
        this.nome = nome;
    }

    public String getCd_representante() {
        return cd_representante;
    }

    public void setCd_representante(String cd_representante) {
        this.cd_representante = cd_representante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
