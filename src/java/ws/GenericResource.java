package ws;
/*Classe de recursos acessáveis no web service
    - saidas em Json acontecem aqui*/
import classes.Filtro;
import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("generic")
public class GenericResource {

    @Context
    private UriInfo context;

    public GenericResource() {

    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        throw new UnsupportedOperationException();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }

    @GET
    @Produces("application/json")
    @Path("produtocomposicao/{cd_produto}")
    public String composicao(@PathParam("cd_produto") int cd_produto) {

        String string_json = "[";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        try {
            string_json += con.produto_composicao(c.conexao(), cd_produto);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        string_json += "]";
        return string_json;
    }
    
    @GET
    @Produces("application/json")
    @Path("nf_cliente/{cd_cliente}")
    public String nf_cliente(@PathParam("cd_cliente") int cd_cliente) {
        
        String volta = "";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        
        try {
            volta = con.nf_cliente(c.conexao(),cd_cliente);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return volta;
    }

    @GET
    @Produces("application/json")
    @Path("produtosobra/{cd_produto},{cd_empresa}")
    public String sobra(@PathParam("cd_produto") int cd_produto, @PathParam("cd_empresa") int cd_empresa) {

        String volta = "";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        try {
            volta = con.produto_sobra(c.conexao(), cd_produto, cd_empresa);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return volta;
    }

    @GET
    @Produces("application/json")
    @Path("produtosobra_ponta/{cd_produto}")
    public String sobra_ponta(@PathParam("cd_produto") int cd_produto) {
        String volta = "";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        try {
            volta = con.produto_ponta_estoque(c.conexao(), cd_produto);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return volta;
    }

    @GET
    @Produces("application/json")
    @Path("representantes_estados/{uf},{marca}")
    public String representantes_estados(@PathParam("uf") String uf, @PathParam("marca") String marca) {
        String volta = "";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        try {
            volta = con.representantes_estado(c.conexao(), uf, marca);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return volta;
    }
    
    @GET
    @Produces("application/json")
    @Path("nf_sigep/{dia},{mes},{ano}")
    public String nf_sigep(@PathParam("dia") String dia, @PathParam("mes") String mes,@PathParam("ano") String ano) {
        String volta = "";
        
        ConexaoSigep conexao = new ConexaoSigep();
        
        String nova_data = mes+"/"+dia+"/"+ano;
        
        volta = conexao.conexao(nova_data);
        
        return volta;
    }

    @GET
    @Produces("application/json")
    @Path("historico_pedido/{json}")
    public String representantes_estados(@PathParam("json") String json) {
        Filtro pessoa = new Gson().fromJson(json, Filtro.class);

        String dt_inicial = pessoa.getDt_inicial();
        String dt_final = pessoa.getDt_final();

        int colecao = pessoa.getColecao();
        int tp_situacao[] = pessoa.getTp_situacao();
        int marca = pessoa.getMarca();
        int cd_represent = pessoa.getCd_represent();

        String volta = "";
        conexaoOracle c = new conexaoOracle();

        consultas con = new consultas();
        try {
            volta = con.pedidos_representante(c.conexao(), dt_inicial, dt_final, colecao, tp_situacao, marca, cd_represent);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return volta;
    }
}
