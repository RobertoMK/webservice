package ws;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class conexaoOracle {

    ResultSet rs = null;
    Statement stmt = null;

    public conexaoOracle() {
    }

    public Connection conexao() {
        try {
            String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
            String JDBC_STRING = "jdbc:oracle:thin:@192.168.200.251:1521/ORAPW";
            String USER_NAME = "BIPWBRASIL";
            String PASSWD = "un89v34h";

            try {
                Class.forName(JDBC_DRIVER);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(conexaoOracle.class.getName()).log(Level.SEVERE, null, ex);
            }
            Connection conn = DriverManager.getConnection(JDBC_STRING, USER_NAME, PASSWD);
            if (conn == null) {
                //JOptionPane.showMessageDialog(null, "NÃO");
                return conn;
            } else {
                //JOptionPane.showMessageDialog(null, "sim");
                return conn;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
