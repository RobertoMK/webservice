package ws;

import classes.Composicao;
import classes.NotaFiscal;
import classes.Pedido;
import classes.ProdutoComposicao;
import classes.ProdutoSobra;
import classes.Representantes;
import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class consultas {

    public consultas() {
    }

    public String nf_cliente(Connection conn, int cd_cliente) throws SQLException {

        ArrayList<NotaFiscal> notas = new ArrayList<NotaFiscal>();

        String sql = "SELECT \n"
                + "A.NR_NF\n"
                + "FROM\n"
                + "    UPWBRASIL.VR_FIS_NF A\n"
                + "WHERE\n"
                + "    A.CD_EMPFAT = 6\n"
                + "AND \n"
                + "    A.CD_PESSOA ="+cd_cliente+"\n";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {
            NotaFiscal nota = new NotaFiscal();
            nota.setNr_nf(rs.getString("NR_NF"));
            notas.add(nota);
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(notas);
    }

    public String produto_sobra(Connection conn, int cd_produto, int cd_empresa) throws SQLException {
        Integer Contador = 0;
        String nome = "";
        ProdutoSobra produto = new ProdutoSobra();

        String sql = "SELECT (UPWBRASIL.F_DIC_PRD_NIVEL(A.CD_PRODUTO, 'CD')) as VL_FORMULA_1,A.CD_PRODUTO,SUM(A.QT_MOVIMENTADA) as QT_MOVIMENTADA,\n"
                + "(UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CC')) as DS_COR,\n"
                + "(UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CT')) as DS_TAMANHO,\n"
                + "(CASE WHEN( SUM (A.QT_MOVIMENTADA) - MIN ((UPWBRASIL.F_DIC_QTD_PED_PEDIDO(A.CD_EMPRESA, NULL, NULL, NULL, NULL,NULL, A.CD_PRODUTO, NULL, NULL, 'P'))) < 0)\n"
                + "                THEN 0 ELSE SUM (A.QT_MOVIMENTADA) - MIN ((UPWBRASIL.F_DIC_QTD_PED_PEDIDO(A.CD_EMPRESA, NULL, NULL, NULL, NULL,NULL, A.CD_PRODUTO, NULL, NULL, 'P'))) END) as DS_SOBRA\n"
                + "                from  UPWBRASIL.VR_PRD_KARDEX A\n"
                + "                where  A.CD_SALDO =1 and A.QT_MOVIMENTADA <>0 and A.CD_PRODUTO = " + cd_produto + " AND CD_EMPRESA = " + cd_empresa + "\n"
                + "                group by  (UPWBRASIL.F_DIC_PRD_NIVEL(A.CD_PRODUTO, 'CD')), A.CD_PRODUTO, (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CC')), (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CT'))\n"
                + "                having  SUM(A.QT_MOVIMENTADA) <>0 order by  VL_FORMULA_1";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {
            produto.setCd_produto(rs.getString("CD_PRODUTO"));
            produto.setVl_formula_1(rs.getString("VL_FORMULA_1"));
            produto.setQt_movimentada(rs.getString("QT_MOVIMENTADA"));
            produto.setDs_cor(rs.getString("DS_COR"));
            produto.setDs_tamanho(rs.getString("DS_TAMANHO"));
            produto.setDs_sobra(rs.getString("DS_SOBRA"));

            Contador++;
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(produto);
    }

    public String produto_composicao(Connection conn, int id) throws SQLException {
        Integer Contador = 0;
        ProdutoComposicao produto = new ProdutoComposicao();
        ArrayList<Composicao> comp = new ArrayList<Composicao>();

        String sql = "SELECT CD_PRODUTO, PR_COMPOSICAO, DS_FIBRA FROM UPWBRASIL.VR_PRD_COMPOSICAO WHERE CD_PRODUTO =" + id;
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {

            produto.setCd_produto(rs.getString("CD_PRODUTO"));
            Composicao compo = new Composicao();
            compo.setDs_fibra(rs.getString("DS_FIBRA"));
            compo.setPr_composicao(Integer.parseInt(rs.getString("PR_COMPOSICAO")));

            comp.add(compo);
            produto.setComp(comp);
            Contador++;
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(produto);
    }

    public String produto_ponta_estoque(Connection conn, int cd_produto) throws SQLException {
        Integer Contador = 0;
        String nome = "";
        ProdutoSobra produto = new ProdutoSobra();

        String sql = "SELECT (UPWBRASIL.F_DIC_PRD_NIVEL(A.CD_PRODUTO, 'CD')) as VL_FORMULA_1,\n"
                + "                        A.CD_PRODUTO,\n"
                + "                        SUM(A.QT_MOVIMENTADA) as QT_MOVIMENTADA,\n"
                + "                        (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CC')) as DS_COR,\n"
                + "                        (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CT')) as DS_TAMANHO,\n"
                + "                        (CASE WHEN( SUM (A.QT_MOVIMENTADA) - MIN ((UPWBRASIL.F_DIC_QTD_PED_PEDIDO(A.CD_EMPRESA, NULL, NULL, NULL, NULL,NULL, A.CD_PRODUTO, NULL, NULL, 'P'))) < 0)\n"
                + "                         THEN 0 ELSE SUM (A.QT_MOVIMENTADA) - MIN ((UPWBRASIL.F_DIC_QTD_PED_PEDIDO(A.CD_EMPRESA, NULL, NULL, NULL, NULL,NULL, A.CD_PRODUTO, NULL, NULL, 'P'))) END) as DS_SOBRA  \n"
                + "                from  UPWBRASIL.VR_PRD_KARDEX A\n"
                + "                where  A.CD_SALDO =13 and A.CD_PRODUTO = " + cd_produto + "and A.QT_MOVIMENTADA <>0 AND A.CD_EMPRESA = 2\n"
                + "                group by  (UPWBRASIL.F_DIC_PRD_NIVEL(A.CD_PRODUTO, 'CD')), A.CD_PRODUTO, (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CC')), (UPWBRASIL.F_DIC_BUSCA_CORTAM_PRD(A.CD_PRODUTO, 'CT')) \n"
                + "                having  SUM(A.QT_MOVIMENTADA) <>0 order by  VL_FORMULA_1";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {
            produto.setCd_produto(rs.getString("CD_PRODUTO"));
            produto.setVl_formula_1(rs.getString("VL_FORMULA_1"));
            produto.setQt_movimentada(rs.getString("QT_MOVIMENTADA"));
            produto.setDs_cor(rs.getString("DS_COR"));
            produto.setDs_tamanho(rs.getString("DS_TAMANHO"));
            produto.setDs_sobra(rs.getString("DS_SOBRA"));

            Contador++;
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(produto);
    }

    public String representantes_estado(Connection conn, String uf, String marca) throws SQLException {
        ArrayList<Representantes> repre = new ArrayList<Representantes>();

        String sql = "SELECT\n"
                + "    A.CD_REPRESENTANT,\n"
                + "    B.NM_PESSOA,\n"
                + "    C.DS_CLASSIFICACAO,\n"
                + "    B.DS_UF\n"
                + "FROM\n"
                + "    UPWBRASIL.VR_PED_PEDIDOC A,\n"
                + "    UPWBRASIL.VR_PESSOAEND B,\n"
                + "    UPWBRASIL.VR_PED_PEDIDOCLAS C\n"
                + "WHERE\n"
                + "    A.DT_PEDIDO >= '01-01-2018'\n"
                + "AND \n"
                + "    A.CD_REPRESENTANT = B.CD_PESSOA\n"
                + "AND \n"
                + "    A.CD_EMPRESA = C.CD_EMPRESA\n"
                + "AND \n"
                + "    A.CD_PEDIDO = C.CD_PEDIDO\n"
                + "AND \n"
                + "    A.CD_REPRESENTANT = C.CD_REPRESENTANT\n"
                + "AND \n"
                + "    C.CD_TIPOCLAS = 1\n"
                + "AND \n"
                + "    C.CD_CLASSIFICACAO = '" + marca + "'\n"
                + "AND \n"
                + "    B.DS_UF = '" + uf + "'\n"
                + "AND\n"
                + "    A.CD_REPRESENTANT NOT IN(100000011,100000035,26115,26116,26117,7723,100000013,100000037,1386)\n"
                + "GROUP BY\n"
                + "    B.NM_PESSOA,A.CD_REPRESENTANT,C.DS_CLASSIFICACAO,B.DS_UF\n"
                + "ORDER BY\n"
                + "    A.CD_REPRESENTANT";
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {
            Representantes representante = new Representantes();
            representante.setCd_representante(rs.getString("CD_REPRESENTANT"));
            representante.setNome(rs.getString("NM_PESSOA"));
            repre.add(representante);
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(repre);
    }

    public String pedidos_representante(Connection conn, String dt_inicial, String dt_final, int colecao, int tp_situacao[], int marca, int cd_represent) throws SQLException {
        ArrayList<Pedido> pedidos = new ArrayList<>();
        String tp = "";

        for (int i = 0; i < tp_situacao.length; i++) {
            tp += tp_situacao[i];
            if (i != tp_situacao.length - 1) {
                tp += ",";
            }
        }

        String sql = "SELECT\n"
                + "     A.CD_EMPRESA AS CODIGO_EMPRESA,\n"
                + "     UPWBRASIL.F_DIC_PES_NOME(A.CD_REPRESENTANT) AS REPRE,\n"
                + "     A.CD_PEDIDO AS CODIGO_PEDIDO,\n"
                + "     A.NR_PEDIDOCLIENTE AS NUMERO_PEDIDO,\n"
                + "     A.QT_SOLICITADA AS QUANT_SOLICITADA,\n"
                + "     A.QT_ATENDIDA AS QUANT_ATENDIDA,\n"
                + "     A.QT_EXTRA AS QUANT_EXTRA,\n"
                + "     A.QT_CANCELADA AS QUANT_CANCELADA,\n"
                + "     A.QT_PENDENTE AS QUANT_PENDENTE,\n"
                + "     A.QT_SOLICITADA AS QUANT_REAL,\n"
                + "     A.VL_SOLICITADO AS VALOR_SOLICITADO,\n"
                + "     A.VL_SOLICITADONEG AS VALOR_SOLICITADONEG,\n"
                + "     A.VL_ATENDIDO AS VALOR_ATENDIDO,\n"
                + "     A.VL_ATENDIDONEG AS VALOR_ATENDIDONEG,\n"
                + "     A.VL_CANCELADO AS VALOR_CANCELADO,\n"
                + "     A.VL_CANCELADONEG AS VALOR_CANCELADONEG,\n"
                + "     A.VL_PENDENTE AS VALOR_PENDENTE,\n"
                + "     A.VL_PENDENTENEG AS VALOR_PENDENTENEG,\n"
                + "     A.VL_EXTRA AS VALOR_EXTRA,\n"
                + "     A.VL_EXTRANEG AS VALOR_EXTRANEG,\n"
                + "     A.VL_SOLICITADO AS VALOR_REAL,\n"
                + "     A.VL_SOLICITADONEG AS VALOR_REALNEG,\n"
                + "     UPWBRASIL.F_DIC_PED_CLASSIFICACAO(A.CD_EMPRESA,A.CD_PEDIDO,2,'DS'),\n"
                + "     UPWBRASIL.F_DIC_PES_NOME(A.CD_CLIENTE) AS NOME_CLIENTE,\n"
                + "     UPWBRASIL.F_DIC_PES_CPFCNPJ(A.CD_CLIENTE) AS CNPJ_CLIENTE,\n"
                + "     A.DT_PEDIDO AS DATA_PEDIDO,\n"
                + "     A.DT_PREVBAIXA DATA_FATURAMENTO,\n"
                + "     A.TP_SITUACAO AS SITUACAO,\n"
                + "     A.NR_ITEM AS QUANT_ITEM\n"
                + "FROM \n"
                + "    UPWBRASIL.VR_PED_PEDIDOC A\n"
                + "WHERE\n"
                + "    A.CD_REPRESENTANT = '" + cd_represent + "'\n"
                + "AND\n"
                + "    A.CD_EMPRESA = 1\n"
                + "AND\n"
                + "    UPWBRASIL.F_DIC_PED_CLASSIFICACAO(A.CD_EMPRESA,A.CD_PEDIDO,1,'CD') IN(" + marca + ")\n"
                + "AND\n"
                + "    UPWBRASIL.F_DIC_PED_CLASSIFICACAO(A.CD_EMPRESA,A.CD_PEDIDO,2,'CD') IN(" + colecao + ")\n"
                + "AND \n"
                + "    A.TP_SITUACAO IN(" + tp + ")\n"
                + "AND\n"
                + "    A.DT_PEDIDO >= '" + dt_inicial + "'\n"
                + "AND\n"
                + "    A.DT_PEDIDO <= '" + dt_final + "'\n"
                + "GROUP BY\n"
                + "     A.CD_EMPRESA ,\n"
                + "     UPWBRASIL.F_DIC_PES_NOME(A.CD_REPRESENTANT),\n"
                + "     A.CD_PEDIDO ,\n"
                + "     A.NR_PEDIDOCLIENTE ,\n"
                + "     A.QT_SOLICITADA ,\n"
                + "     A.QT_ATENDIDA ,\n"
                + "     A.QT_EXTRA ,\n"
                + "     A.QT_CANCELADA ,\n"
                + "     A.QT_PENDENTE ,\n"
                + "     A.QT_SOLICITADA ,\n"
                + "     A.VL_SOLICITADO ,\n"
                + "     A.VL_SOLICITADONEG ,\n"
                + "     A.VL_ATENDIDO ,\n"
                + "     A.VL_ATENDIDONEG ,\n"
                + "     A.VL_CANCELADO ,\n"
                + "     A.VL_CANCELADONEG ,\n"
                + "     A.VL_PENDENTE ,\n"
                + "     A.VL_PENDENTENEG ,\n"
                + "     A.VL_EXTRA ,\n"
                + "     A.VL_EXTRANEG,\n"
                + "     A.VL_SOLICITADO ,\n"
                + "     A.VL_SOLICITADONEG ,\n"
                + "     UPWBRASIL.F_DIC_PED_CLASSIFICACAO(A.CD_EMPRESA,A.CD_PEDIDO,2,'DS'),\n"
                + "     UPWBRASIL.F_DIC_PES_NOME(A.CD_CLIENTE) ,\n"
                + "     UPWBRASIL.F_DIC_PES_CPFCNPJ(A.CD_CLIENTE),\n"
                + "     A.DT_PEDIDO,\n"
                + "     A.DT_PREVBAIXA,\n"
                + "     A.TP_SITUACAO,\n"
                + "     A.NR_ITEM\n"
                + "ORDER BY \n"
                + "    A.CD_PEDIDO";

        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (rs.next()) {
            Pedido pedido = new Pedido();
            pedido.setCd_empresa(rs.getString("CODIGO_EMPRESA"));
            pedido.setNome_repre(rs.getString("REPRE"));
            pedido.setCd_pedido(rs.getString("CODIGO_PEDIDO"));
            pedido.setPed_cliente(rs.getString("NUMERO_PEDIDO"));
            pedido.setQt_solicitada(rs.getString("QUANT_SOLICITADA"));
            pedido.setQt_atendida(rs.getString("QUANT_ATENDIDA"));
            pedido.setQt_extra(rs.getString("QUANT_EXTRA"));
            pedido.setQt_cancelada(rs.getString("QUANT_CANCELADA"));
            pedido.setQt_pedente(rs.getString("QUANT_PENDENTE"));
            pedido.setQt_real(rs.getString("QUANT_REAL"));
            pedido.setVl_solicitado(rs.getString("VALOR_SOLICITADO"));
            pedido.setVl_solicitadoneg(rs.getString("VALOR_SOLICITADONEG"));
            pedido.setVl_atendido(rs.getString("VALOR_ATENDIDO"));
            pedido.setVl_atendidoneg("VALOR_ATENDIDONEG");
            pedido.setVl_cancelado(rs.getString("VALOR_CANCELADO"));
            pedido.setVl_canceladoneg(rs.getString("VALOR_CANCELADONEG"));
            pedido.setVl_pedente(rs.getString("VALOR_PENDENTE"));
            pedido.setVl_pedenteneg(rs.getString("VALOR_PENDENTENEG"));
            pedido.setVl_extra(rs.getString("VALOR_EXTRA"));
            pedido.setVl_extraneg(rs.getString("VALOR_EXTRANEG"));
            pedido.setVl_realneg(rs.getString("VALOR_REALNEG"));
            pedido.setVl_real("VALOR_REAL");
            pedido.setVl_realneg("VALOR_REALNEG");
            pedido.setColecao("COLECAO");
            pedido.setNm_cliente(rs.getString("NOME_CLIENTE"));
            pedido.setCpnj_cliente(rs.getString("CNPJ_CLIENTE"));
            pedido.setDt_pedido(rs.getString("DATA_PEDIDO"));
            pedido.setDt_prev_fat(rs.getString("DATA_FATURAMENTO"));
            pedido.setTp_situacao(rs.getString("SITUACAO"));
            pedido.setNr_item(rs.getString("QUANT_ITEM"));
            pedidos.add(pedido);
        }

        stmt.close();

        Gson g = new Gson();
        conn.close();

        return g.toJson(pedidos);
    }
}
