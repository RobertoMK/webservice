package ws;

import classes.rastreio_sigep;
import com.google.gson.Gson;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ConexaoSigep {

    public ConexaoSigep() {
    }

    public String conexao(String dat) {
        Connection con = null;
        try {
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            // Nome do arquivo
            String filename = "C:/Correios_PW_UNI/SIGEP/SIGEP.mdb";

            File arquivo = new File(filename);

            // Verifica se o arquivo não existe.
            if (!arquivo.exists()) {
                System.err.println("Arquivo não existe!");
            }

            String database = "jdbc:ucanaccess://" + filename.trim();

            System.out.println(database);

            // Realiza a conexão com o banco de dados
            con = DriverManager.getConnection(database);

            String sql = "select lista_data, objetonacional_etiqueta, destinatario_codigo, notafiscal_codigo "
                    + "from ObjetoNacional "
                    + "where lista_data between #"+dat+"# and #"+dat+"#";

            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet res = ps.executeQuery();

            ArrayList<rastreio_sigep> itens = new ArrayList();

            int i = 0;

            while (res.next()) {
                Date data = res.getDate("lista_data");
                String objetonacional_etiqueta = res.getString("objetonacional_etiqueta");
                int destinatario_codigo = res.getInt("destinatario_codigo");
                int notafiscal_codigo = res.getInt("notafiscal_codigo");
                int nm_nf = 0;

                String sql2 = "select notafiscal_numero from NotaFiscal where notafiscal_codigo = " + notafiscal_codigo;

                PreparedStatement ps2 = con.prepareStatement(sql2);

                ResultSet res2 = ps2.executeQuery();

                while (res2.next()) {
                    nm_nf = res2.getInt("notafiscal_numero");
                }

                rastreio_sigep sigep = new rastreio_sigep();

                sigep.setData(data.toString());
                sigep.setRastreio(objetonacional_etiqueta);
                sigep.setCod_destinatario(Integer.toString(destinatario_codigo));
                sigep.setCod_nf(Integer.toString(nm_nf));

                itens.add(sigep);
            }

            res.close();
            con.close();

            Gson g = new Gson();

            return g.toJson(itens);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
